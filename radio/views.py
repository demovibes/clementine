from string import ascii_lowercase

from django.views.generic import ListView

from .models import Artist, Platform, Song


class ArtistsPageView(ListView):
    paginate_by = 30
    context_object_name = "artists_list"

    def get_queryset(self):
        letter = self.kwargs.get("letter", "")
        if letter not in ascii_lowercase:
            return Artist.objects.filter(handle__iregex=r"^[^a-zA-Z].*$")

        return Artist.objects.filter(handle__startswith=letter)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["alphabet"] = ascii_lowercase + "-"

        return context


class PlatformsPageView(ListView):
    context_object_name = "platforms_list"
    model = Platform


class SongsPageView(ListView):
    paginate_by = 30
    context_object_name = "songs_list"

    def get_queryset(self):
        letter = self.kwargs.get("letter", "")
        if letter not in ascii_lowercase:
            return Song.objects.filter(title__iregex=r"^[^a-zA-Z].*$")

        return Song.objects.filter(title__startswith=letter)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["alphabet"] = ascii_lowercase + "-"

        return context
