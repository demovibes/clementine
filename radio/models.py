from django.contrib.auth.models import User
from django.db import models

from core.models import MetaModel, Status


class Platform(models.Model):
    name = models.CharField(max_length=64, unique=True)
    description = models.TextField()
    symbol = models.ImageField(upload_to="platforms/symbols", blank=True, null=True)
    image = models.ImageField(upload_to="platforms/images", blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class Label(MetaModel):
    name = models.CharField(
        max_length=40,
        unique=True,
        db_index=True,
        verbose_name="* Name",
        help_text="Name of this label, as you want it to appear on the site",
    )
    found_date = models.DateField(
        help_text="Date label was formed (YYYY-MM-DD)", null=True, blank=True
    )
    cease_date = models.DateField(
        help_text="Date label was closed/went out of business (YYYY-MM-DD)",
        null=True,
        blank=True,
    )
    info = models.TextField(
        blank=True, help_text="Additional information about this label. No HTML."
    )
    pouetid = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="Pouet ID",
        help_text="If this label has a pouet group entry, enter the ID here.",
    )
    hol_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="H.O.L. ID",
        help_text="Hall of Light ID number (Amiga) - See http://hol.abime.net",
    )
    label_icon = models.ImageField(
        upload_to="media/labels/icons",
        blank=True,
        null=True,
        verbose_name="Label Icon (Shows instead of default icon)",
        help_text="Upload an image containing the icon for this label",
    )
    logo = models.ImageField(
        upload_to="media/labels",
        blank=True,
        null=True,
        verbose_name="Label Logo",
        help_text="Upload an image containing the logo for this label",
    )
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    webpage = models.URLField(
        blank=True,
        verbose_name="Website",
        help_text="Website for this label, if available",
    )
    wiki_link = models.URLField(
        blank=True, help_text="Full URL to wikipedia entry (if available)"
    )
    # ~ links = generic.GenericRelation(GenericLink)

    def __str__(self):
        return self.name


class License(models.Model):
    name = models.CharField(max_length=50, verbose_name="Name")
    description = models.TextField(verbose_name="Description", blank=True)
    icon = models.ImageField(
        upload_to="licenses", blank=True, null=True, verbose_name="License icon"
    )
    url = models.URLField(verbose_name="License URL", null=True, blank=True)
    downloadable = models.BooleanField(default=False, verbose_name="Can be downloaded")

    def __str__(self):
        return self.name


class Group(MetaModel):
    name = models.CharField(
        max_length=80,
        unique=True,
        db_index=True,
        verbose_name="* Name",
        help_text="The name of this group as you want it to appear.",
    )
    found_date = models.DateField(
        verbose_name="Found Date",
        help_text="Date this group was formed (YYYY-MM-DD)",
        null=True,
        blank=True,
    )
    group_icon = models.ImageField(
        help_text="Group Icon (Shows instead of default icon)",
        upload_to="media/groups/icons",
        blank=True,
        null=True,
    )
    group_logo = models.ImageField(
        help_text="Logo/Pic Of This Group",
        upload_to="media/groups",
        blank=True,
        null=True,
    )
    info = models.TextField(
        blank=True,
        verbose_name="Group Info",
        help_text="Additional information on this group. No HTML.",
    )
    pouetid = models.IntegerField(
        verbose_name="Pouet ID",
        help_text="If this group has a Pouet entry, enter the ID number here - See http://www.pouet.net",
        blank=True,
        null=True,
    )
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    webpage = models.URLField(
        blank=True,
        verbose_name="Website",
        help_text="Add the website address for this group, if one exists.",
    )
    wiki_link = models.URLField(
        blank=True, help_text="URL to wikipedia entry (if available)"
    )

    def __str__(self):
        return self.name


class Artist(MetaModel):
    handle = models.CharField(
        max_length=64,
        unique=True,
        db_index=True,
        verbose_name="* Handle",
        help_text="Artist handle/nickname. If no handle is used, place their real name here (and not in the above real name position) to avoid duplication",
    )
    name = models.CharField(
        max_length=64,
        blank=True,
        verbose_name="Name",
        help_text="Artist name (First and Last)",
    )
    alias_of = models.ForeignKey(
        "self", null=True, blank=True, related_name="aliases", on_delete=models.CASCADE
    )
    artist_pic = models.ImageField(
        verbose_name="Picture",
        help_text="Insert a picture of this artist.",
        upload_to="artists",
        blank=True,
        null=True,
    )
    deceased_date = models.DateField(
        help_text="Date of Passing (YYYY-MM-DD)",
        null=True,
        blank=True,
        verbose_name="Date Of Death",
    )
    dob = models.DateField(
        help_text="Date of Birth (YYYY-MM-DD)", null=True, blank=True
    )
    groups = models.ManyToManyField(
        Group,
        blank=True,
        help_text="Select any known groups this artist is a member of.",
    )
    hol_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="H.O.L. ID",
        help_text="Hall of Light Artist ID number (Amiga) - See http://hol.abime.net",
    )
    home_country = models.CharField(
        blank=True,
        max_length=10,
        verbose_name="Country Code",
        help_text="Standard country code, such as gb, us, ru, se etc.",
    )
    home_location = models.CharField(
        blank=True,
        max_length=40,
        verbose_name="Location",
        help_text="Hometown location, if known.",
    )
    info = models.TextField(
        blank=True, help_text="Additional artist information. No HTML allowed."
    )
    labels = models.ManyToManyField(
        Label,
        blank=True,
        help_text="Select any known production labels associated with this artist.",
    )  # Production labels this artist has worked for
    last_fm_id = models.CharField(
        blank=True,
        max_length=32,
        verbose_name="Last.fm ID",
        help_text="If this artist has a Last.FM account, specify the username portion here. Use + instead of Space. Example: Martin+Galway",
    )
    link_to_user = models.OneToOneField(
        User, null=True, blank=True, on_delete=models.SET_NULL, related_name="User"
    )
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    twitter_id = models.CharField(
        blank=True,
        max_length=32,
        verbose_name="Twitter ID",
        help_text="Enter the Twitter account name of the artist, if known (without the Twitter URL)",
    )
    webpage = models.URLField(
        blank=True,
        verbose_name="Website",
        help_text="Website for this artist. Must exist on the web.",
    )
    wiki_link = models.URLField(
        blank=True, help_text="URL to Wikipedia entry (if available)"
    )
    scene_relevance = models.TextField(
        blank=True,
        help_text="The artist's demoscene relevance. So the mods can approve it faster",
    )

    def __str__(self):
        return f"{self.id} - {self.handle}"

    @property
    def is_deceased(self):
        return self.deceased_date is not None


class Song(models.Model):
    LEGACY_FLAG = (
        (
            " ",
            "N/A",
        ),  # Is not a legacy tune (new upload since Jul. 2018, or replaced streamrip)
        (
            "R",
            "Recovered",
        ),  # is a recovered stream rip (not original, needs replacement)
        ("M", "Missing"),  # existed in Old Necta but is present now
    )

    title = models.CharField(
        verbose_name="* Song Name",
        help_text="The name of this song, as it should appear in the database",
        max_length=80,
        db_index=True,
    )
    bitrate = models.IntegerField(blank=True, null=True)
    explicit = models.BooleanField(
        default=False,
        verbose_name="Explicit Lyrics?",
        help_text="Place a checkmark in the box to flag this song as having explicit lyrics/content",
    )
    file = models.FileField(
        upload_to="media/songs",
        verbose_name="File",
        max_length=200,
        help_text="Select a module (mod, xm, etc...) or audio file (mp3, ogg, etc...) to upload. See FAQ for details.",
    )
    locked_until = models.DateTimeField(blank=True, null=True)
    loopfade_time = models.PositiveIntegerField(
        default=0, verbose_name="Forced play time", help_text="In seconds, 0 = disabled"
    )
    playback_fadeout = models.BooleanField(
        default=True,
        verbose_name="Fadeout at end",
        help_text="Only active if Forced play time is set",
    )
    playback_bass_mode = models.CharField(
        max_length=4,
        choices=(
            ("pt1", "ProTracker 1"),
            ("ft2", "FastTracker2"),
            ("bass", "Bass"),
        ),
        blank=True,
        verbose_name="Playback mode",
    )
    playback_bass_inter = models.CharField(
        max_length=6,
        choices=(("off", "Off"), ("linear", "Linear"), ("sinc", "Sinc")),
        blank=True,
        verbose_name="Playback interpolation",
    )
    playback_bass_mix = models.CharField(
        max_length=4,
        choices=(("Auto", "auto"), ("0.0", "Off"), ("0.3", "Mix"), ("0.5", "Mono")),
        blank=True,
        verbose_name="Playback Mix",
    )
    playback_bass_ramp = models.CharField(
        max_length=10,
        choices=(("off", "Off"), ("normal", "Normal"), ("sensitive", "Sensitive")),
        blank=True,
        verbose_name="Playback RAMPAGE!",
    )
    num_favorited = models.IntegerField(default=0)
    rating = models.FloatField(blank=True, null=True)
    rating_total = models.IntegerField(default=0)
    rating_votes = models.IntegerField(default=0)
    replay_gain = models.FloatField(default=0, verbose_name="Replay gain")
    samplerate = models.IntegerField(blank=True, null=True)
    song_length = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    times_played = models.IntegerField(null=True, default=0)
    pouetlink = models.CharField(max_length=200, blank=True)
    pouetss = models.CharField(max_length=100, blank=True)
    pouetgroup = models.CharField(max_length=100, blank=True)
    pouettitle = models.CharField(max_length=100, blank=True)
    license = models.ForeignKey(
        License, blank=True, null=True, on_delete=models.SET_NULL
    )
    artists = models.ManyToManyField(
        Artist,
        blank=True,
        help_text="Select any known artist this song was written by.",
    )
    groups = models.ManyToManyField(
        Group,
        blank=True,
        help_text="Select any known groups as author of this song.",
    )

    def __str__(self):
        return self.title


class CompilationSongList(models.Model):
    song = models.ForeignKey("Song", on_delete=models.CASCADE)
    compilation = models.ForeignKey("Compilation", on_delete=models.CASCADE)
    index = models.PositiveIntegerField(default=0, verbose_name="Song index")

    class Meta:
        ordering = ["index"]


class Compilation(MetaModel):
    name = models.CharField(
        max_length=80,
        unique=True,
        db_index=True,
        verbose_name="* Name",
        help_text="Name of the compilation, as you want it to appear on the site",
    )  # Name of the compilation
    bar_code = models.CharField(
        help_text="UPC / Bar Code Of Product", max_length=30, blank=True
    )  # Optional bar code number for CD
    comp_icon = models.ImageField(
        help_text="Album Icon (Shows instead of default icon)",
        upload_to="media/compilations/icons",
        blank=True,
        null=True,
    )  # Album Artwork
    cover_art = models.ImageField(
        help_text="Album Cover/Screenshot",
        upload_to="media/compilations",
        blank=True,
        null=True,
    )  # Album Artwork
    details_page = models.URLField(
        help_text="External link to info page about the compilation", blank=True
    )  # Link to external website about the compilation, such as demoparty page
    download_link = models.URLField(
        help_text="Link to download the production (in any format)", blank=True
    )  # Download Link
    hol_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="H.O.L. ID",
        help_text="Hall of Light ID number (Amiga) - See http://hol.abime.net",
    )
    info = models.TextField(
        help_text="Description, as you want it to appear on the site. BBCode tags supported. No HTML.",
        blank=True,
    )  # Info page, which will be a simple textbox entry such as a description field
    label = models.CharField(
        verbose_name="Prod. Label",
        help_text="Production label/Distributor of this compilation. Will appear as [name] by [label]",
        max_length=30,
        blank=True,
    )  # Record label produced under, if applicable (Not always tied to a specific group/artist)
    media_format = models.CharField(
        help_text="Usually CD/DVD/FLAC/MP3/OGG etc.", max_length=30, blank=True
    )  # Optional media format, such as CD/DVD/FLAC/MP3 etc.
    num_discs = models.IntegerField(
        help_text="If this is a media format like CD, you can specify the number of disks",
        blank=True,
        null=True,
    )  # Number of discs in the compilation
    pouet = models.IntegerField(
        help_text="Pouet ID for compilation", blank=True, null=True
    )  # If the production has a pouet ID
    prod_artists = models.ManyToManyField(
        Artist,
        verbose_name="Production Artists",
        help_text="Artists associated with the production of this compilation (not necessarily the same as the tracks)",
        blank=True,
    )  # Internal artists involved in the production
    prod_groups = models.ManyToManyField(
        Group,
        verbose_name="Production Groups",
        help_text="Groups associated with the production of this compilation (not necessarily the same as the tracks)",
        blank=True,
    )  # Internal groups involved in the production
    prod_notes = models.TextField(
        help_text="Production notes, from the author/group/artists specific to the making of this compilation",
        blank=True,
    )  # Personalized production notes
    projecttwosix_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="Project2612 ID",
        help_text="Project2612 ID Number (Genesis / Megadrive) - See http://www.project2612.org",
    )
    purchase_page = models.URLField(
        help_text="If this is a commercial product, you can provide a 'Buy Now' link here",
        blank=True,
    )  # If commercial CD, link to purchase the album
    rel_date = models.DateField(
        help_text="Original Release Date", null=True, blank=True
    )  # Original release date, we could also add re-release date though not necessary just yet!
    running_time = models.IntegerField(
        help_text="Overall running time (In Seconds)", blank=True, null=True
    )  # Running time of the album/compilation
    songs = models.ManyToManyField(Song, blank=True, through="CompilationSongList")
    startswith = models.CharField(
        max_length=1, editable=False, db_index=True
    )  # Starting letter for search purposes
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    wiki_link = models.URLField(
        blank=True, help_text="URL to wikipedia entry (if available)"
    )
    youtube_link = models.URLField(
        help_text="Link to Youtube/Google Video Link (external)", blank=True
    )  # Link to a video of the production
    zxdemo_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="ZXDemo ID",
        help_text="ZXDemo Production ID Number (Spectrum) - See http://www.zxdemo.org",
    )
    # ~ screenshots = generic.GenericRelation(ScreenshotObjectLink)

    def __str__(self):
        return self.name
