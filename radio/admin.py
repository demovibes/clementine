from django.contrib import admin

from .models import Artist, Compilation, Group, Label, License, Platform, Song


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    list_display = ["id", "handle", "name"]
    list_per_page = 20
    filter_horizontal = ["groups", "labels"]


@admin.register(Compilation)
class CompilationAdmin(admin.ModelAdmin):
    filter_horizontal = ["prod_artists", "prod_groups"]


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_per_page = 20


@admin.register(Label)
class LabelAdmin(admin.ModelAdmin):
    pass


@admin.register(License)
class LicenseAdmin(admin.ModelAdmin):
    pass


@admin.register(Platform)
class PlatformAdmin(admin.ModelAdmin):
    pass


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    list_per_page = 20
    filter_horizontal = ["artists", "groups"]
