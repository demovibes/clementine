from django.urls import path

from radio.views import ArtistsPageView, PlatformsPageView, SongsPageView


app_name = "radio"
urlpatterns = [
    path("artists", ArtistsPageView.as_view(), name="artists_list"),
    path("artists/<letter>", ArtistsPageView.as_view(), name="artists_list"),
    path("platforms", PlatformsPageView.as_view(), name="platforms_list"),
    path("songs", SongsPageView.as_view(), name="songs_list"),
    path("songs/<letter>", SongsPageView.as_view(), name="songs_list"),
]
