import json
import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from radio.models import Artist, Group, Song


class Command(BaseCommand):
    help = "Import artists from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "artists",
            help="Path to artists folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            artists = Artist.objects.all()
            for artist in artists:
                try:
                    if artist.artist_pic:
                        os.remove(artist.artist_pic.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
            artists.delete()

        self.stdout.write("Importing artists")
        artists_path = os.path.join(options["artists"], "json")
        for artist_filename in sorted(
            os.listdir(artists_path),
            key=lambda x: int(x.split(".")[0][7:])
        ):
            artist_path = os.path.join(artists_path, artist_filename)
            with open(artist_path) as file_handler:
                data = json.load(file_handler)
            self.stdout.write(artist_filename)
            self.stdout.flush()

            artist = Artist(
                id=int(data["id"]),
                handle=data["handle"],
                name=data["name"] or "",
                webpage=data["website"] or "",
                home_country=data["country"] or "",
                home_location=data["location"] or "",
                info=data["info"] or "",
                twitter_id=data["twitter"] or "",
                last_fm_id=data["lastfm"] or "",
            )
            if data["picture"] != "no_artist_pic.png":
                artist_picture_path = os.path.join(options["artists"], data["picture"])
                try:
                    artist_file = open(artist_picture_path, mode="rb")
                    artist.artist_pic.save(data["picture"], File(artist_file))
                except FileNotFoundError:
                    pass

            artist.save()

            if data["aliases"] != []:
                for alias in data["aliases"]:
                    try:
                        artist_alias = Artist.objects.get(id=alias["id"])
                    except Artist.DoesNotExist:
                        continue
                    artist.aliases.add(artist_alias)

            if data["groups"] != []:
                for group in data["groups"]:
                    new_group = Group(
                        id=int(group["id"]),
                        name=group["name"],
                    )
                    new_group.save()
                    artist.groups.add(new_group)

            if data["songs"] != []:
                for song in data["songs"]:
                    new_song = Song(
                        id=int(song["id"]),
                        title=song["title"],
                    )
                    new_song.save()
                    new_song.artists.add(artist)

        self.stdout.write("Import finished")
