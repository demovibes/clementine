import json
import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from radio.models import Platform


class Command(BaseCommand):
    help = "Import platforms from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "platforms",
            help="Path to platforms folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            platforms = Platform.objects.all()
            for platform in platforms:
                try:
                    os.remove(platform.symbol.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
                try:
                    os.remove(platform.image.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
            platforms.delete()

        self.stdout.write("Importing platforms")
        platforms_path = os.path.join(options["platforms"], "platforms.json")
        if not os.path.exists(platforms_path):
            raise FileNotFoundError(
                f"File 'platforms.json' not found in {options['platforms']}"
            )
        with open(platforms_path) as file_handler:
            datas = json.load(file_handler)
        for data in datas:
            self.stdout.write(".", ending="")
            self.stdout.flush()

            platform = Platform(
                id=data["id"],
                name=data["name"],
                description=data["description"],
            )
            platform_file_path = os.path.join(options["platforms"], data["symbol"])
            platform_file = open(platform_file_path, mode="rb")
            platform.symbol.save(data["symbol"], File(platform_file))
            platform.save()

        self.stdout.write("")
        self.stdout.write("Import finished")
