import json
import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from radio.models import License


class Command(BaseCommand):
    help = "Import licenses from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "licenses",
            help="Path to licenses folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            licenses = License.objects.all()
            for license in licenses:
                try:
                    os.remove(license.icon.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
                try:
                    os.remove(license.icon.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
            licenses.delete()

        self.stdout.write("Importing licenses")
        licenses_path = os.path.join(options["licenses"], "licenses.json")
        if not os.path.exists(licenses_path):
            raise FileNotFoundError(
                f"File 'licenses.json' not found in {options['licenses']}"
            )
        with open(licenses_path) as file_handler:
            datas = json.load(file_handler)
        for data in datas:
            self.stdout.write(".", ending="")
            self.stdout.flush()

            license = License(
                id=data["id"],
                name=data["name"],
                description=data["description"],
                url=data["url"] or "",
                downloadable=data["downloadable"],
            )
            license_file_path = os.path.join(options["licenses"], data["icon"])
            license_file = open(license_file_path, mode="rb")
            license.icon.save(data["icon"], File(license_file))
            license.save()

        self.stdout.write("")
        self.stdout.write("Import finished")
