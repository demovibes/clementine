import json
import os
import time

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from core.models import Status
from radio.models import Artist, Group, Label, License, Platform, Song


class Command(BaseCommand):
    help = "Import songs from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "songs",
            help="Path to songs folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            songs = Song.objects.all()
            for song in songs:
                try:
                    if song.file:
                        os.remove(song.file.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
            songs.delete()

        self.stdout.write("Importing songs")
        for song_id in range(1, 100)
            prefix = int(song_id / 10000)
            songs_path = os.path.join(options["songs"], "json", str(prefix))
            song_filename = f"song_{song_id}.json"
            song_path = os.path.join(songs_path, song_filename)
            try:
                with open(song_path) as file_handler:
                    data = json.load(file_handler)
            except FileNotFoundError:
                continue
            self.stdout.write(song_filename)
            self.stdout.flush()

            song = Song(
                id=int(data["song_id"]),
                title=data["title"],
                bitrate=data["bitrate"] or "",
                samplerate=data["samplerate"] if data["samplerate"]!="None" else None,
                rating=data["rating"] if data["rating"]!="None" else None,
                rating_votes=data["rating_votes"] or "",
                song_length=int(data["songlength"]) or None,
            )
            if data["locked_until"] != "" and data["locked_until"] != "None":
                song.locked_until = data["locked_until"] + " +0000"
            if data["status_id"] != "":
                status = Status.objects.all().filter(code=data["status_id"])
                if len(status) == 0:
                    status = Status(code=data["status_id"],name=data["status_name"])
                    status.save()
                    song.status = status
                else:
                    song.status = status[0]
            if data["platform_id"] != "":
                platform = Platform.objects.get(id=data["platform_id"])
                if status is not None:
                    song.platform = platform
            if data["license_id"] != "":
                license = License.objects.get(id=data["license_id"])
                if status is not None:
                    song.license = license
            song.save()

            if data["groups"] != []:
                for group in data["groups"]:
                    new_group = Group(
                        id=int(group["id"]),
                        name=group["name"],
                    )
                    new_group.save()
                    song.groups.add(new_group)

            if data["artists"] != []:
                for artist in data["artists"]:
                    new_artist = Artist(
                        id=int(artist["id"]),
                        handle=artist["name"],
                    )
                    new_artist.save()
                    song.artists.add(new_artist)

        self.stdout.write("Import finished")
