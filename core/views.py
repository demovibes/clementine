from django.views.generic import ListView

from .models import News


class HomePageView(ListView):
    def get_queryset(self, **kwargs):
        return News.objects.order_by("-created_at")[:10]
