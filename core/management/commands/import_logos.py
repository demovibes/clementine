import json
import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

from core.models import Logo


class Command(BaseCommand):
    help = "Import logos from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "logos",
            help="Path to logos folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            logos = Logo.objects.all()
            for logo in logos:
                try:
                    os.remove(logo.file.path)
                except FileNotFoundError:
                    # Nothing to remove
                    pass
            logos.delete()
        self.stdout.write("Importing logos")
        logos_path = os.path.join(options["logos"], "logos.json")
        if not os.path.exists(logos_path):
            raise FileNotFoundError(
                f"File 'logos.json' not found in {options['logos']}"
            )
        with open(logos_path) as file_handler:
            datas = json.load(file_handler)
        for data in datas:
            self.stdout.write(".", ending="")
            self.stdout.flush()

            logo = Logo(id=data["index"], creator=data["author"])
            logo_file_name = f"{data['index']}.{data['filename'].split('.')[-1]}"
            logo_file_path = os.path.join(options["logos"], logo_file_name)
            logo_file = open(logo_file_path, mode="rb")
            logo.file.save(data["filename"], File(logo_file))
            logo.save()

        self.stdout.write("")
        self.stdout.write("Import finished")
