from datetime import datetime
import json
import os

from django.core.files import File
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from core.models import News, Status


class Command(BaseCommand):
    help = "Import news from old demovibes project."

    def add_arguments(self, parser):
        # Where files are stored
        parser.add_argument(
            "news",
            help="Path to news folder.",
        )

        # Clean datas before migration
        parser.add_argument(
            "--clean",
            action="store_true",
            help="Clean datas before importing process",
        )

    def handle(self, *args, **options):
        if options["clean"]:
            self.stdout.write("Cleaning datas")
            news = News.objects.all()
            news.delete()
        self.stdout.write("Importing news")
        news_path = os.path.join(options["news"], "news.json")
        if not os.path.exists(news_path):
            raise FileNotFoundError(
                f"File 'news.json' not found in {options['news']}"
            )
        with open(news_path) as file_handler:
            datas = json.load(file_handler)
        for data in datas:
            self.stdout.write(".", ending="")
            self.stdout.flush()

            news = News(id=data["id"], title=data["title"])
            if data["status"] != "":
                status = Status.objects.all().filter(code=data["status"])
                if len(status) == 0:
                    status = Status(code=data["status"])
                    status.save()
                    news.status = status
                else:
                    news.status = status[0]

            content_path = os.path.join(options["news"], f"news_{data['id']}.txt")
            try:
                with open(content_path) as file_handler:
                    news.text = file_handler.read()
            except FileNotFoundError:
                continue
            news.save()
            created_at = datetime.strptime(data["created_at"], "%Y-%m-%d %H:%M")
            news.created_at = timezone.make_aware(
                created_at,
                timezone.get_default_timezone(),
            )
            news.save()

        self.stdout.write("")
        self.stdout.write("Import finished")
