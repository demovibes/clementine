from random import choice

# ~ import re, textwrap

from django import template
from django.conf import settings
from django.core.cache import cache
from django.utils.html import format_html

from core.models import Logo


register = template.Library()


@register.simple_tag
def site_name():
    return settings.SITE_NAME


@register.simple_tag
def logo():
    """
    Returns a random logo
    """
    if not (random_logo := cache.get("current_logo")):
        try:
            logo = choice(Logo.objects.filter(active=True).all())
            title = f"#{logo.id} - By {logo.creator}"
            random_logo = format_html(
                '<img id="logo" src="{}" title="{}" alt="{}" />',
                logo.file.url,
                title,
                title,
            )
        except:
            random_logo = format_html(
                '<img id="logo" src="{}logos/default.png" title="Logo" alt="Logo" />',
                settings.STATIC_URL,
            )
        cache.set("current_logo", random_logo, 60 * 15)

    return random_logo


@register.filter
def bbcode(value):
    """
    Decodes BBcode and replaces it with HTML
    """
    return value
    # ~ for bbset in bbdata_full:
    # ~ p = bbset[0]
    # ~ value = p.sub(bbset[1], value)

    # ~ # The following two code parts handle the more complex list statements
    # ~ temp = ''
    # ~ p = re.compile(r'\[list\](.+?)\[/list\]', re.DOTALL)
    # ~ m = p.search(value)
    # ~ if m:
    # ~ items = re.split(re.escape('[*]'), m.group(1))
    # ~ for i in items[1:]:
    # ~ temp = temp + '<li class="bblist">' + i + '</li>'
    # ~ value = p.sub(r'<ul>'+temp+'</ul>', value)

    # ~ temp = ''
    # ~ p = re.compile(r'\[list=(.)\](.+?)\[/list\]', re.DOTALL)
    # ~ m = p.search(value)
    # ~ if m:
    # ~ items = re.split(re.escape('[*]'), m.group(2))
    # ~ for i in items[1:]:
    # ~ temp = temp + '<li>' + i + '</li>'
    # ~ value = p.sub(r'<ol type="\1">'+temp+'</ol>', value)

    # ~ return value
