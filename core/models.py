from django.contrib.auth.models import User
from django.db import models


class MetaModel(models.Model):
    created_by = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.SET_NULL
    )
    created_at = models.DateTimeField(
        auto_now_add=True, blank=True, null=True
    )
    last_updated = models.DateTimeField(
        auto_now=True, editable=False, blank=True, null=True
    )

    class Meta:
        abstract = True


class Status(models.Model):
    code = models.CharField(max_length=1)
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "Statuses"

    def __str__(self):
        return self.name


class News(MetaModel):
    title = models.CharField(max_length=100)
    text = models.TextField()
    status = models.ForeignKey(Status, null=True, on_delete=models.SET_NULL)
    icon = models.URLField(blank=True)

    class Meta:
        verbose_name_plural = "News"
        ordering = ["-created_at"]

    def __str__(self):
        return self.title


class UserProfile(models.Model):
    VISIBLE_TO = (("A", "All users"), ("R", "Registrered users"), ("N", "No one"))

    user = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    real_name = models.CharField(
        blank=True,
        max_length=40,
        verbose_name="Real Name",
        help_text="Your real name (optional)",
    )
    last_ip = models.GenericIPAddressField(null=True, blank=True, default="")
    aol_id = models.CharField(
        blank=True,
        max_length=40,
        verbose_name="AOL IM",
        help_text="AOL IM ID, for people to contact you (optional)",
    )
    hol_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="H.O.L. ID",
        help_text="If you have a Hall of Light ID number (Amiga Developers) - See http://hol.abime.net",
    )
    icq_id = models.CharField(
        blank=True,
        max_length=40,
        verbose_name="ICQ Number",
        help_text="ICQ Number for people to contact you (optional)",
    )
    yahoo_id = models.CharField(
        blank=True,
        max_length=40,
        verbose_name="Yahoo! ID",
        help_text="Yahoo! IM ID, for people to contact you (optional)",
    )
    twitter_id = models.CharField(
        blank=True,
        max_length=32,
        verbose_name="Twitter ID",
        help_text="Enter your Twitter account name, without the Twitter URL (optional)",
    )
    mastodon_id = models.CharField(
        blank=True,
        max_length=100,
        verbose_name="Mastodon ID",
        help_text="Enter your Mastodon complete account name, with the instance URL (optional)",
    )
    avatar = models.ImageField(upload_to="medias/avatars", blank=True, null=True)
    country = models.CharField(blank=True, max_length=10, verbose_name="Country code")
    location = models.CharField(
        blank=True, max_length=40, verbose_name="Hometown Location"
    )
    custom_css = models.URLField(blank=True)
    email_on_artist_add = models.BooleanField(
        default=True, verbose_name="Send email on artist approval"
    )
    email_on_artist_comment = models.BooleanField(
        default=True, verbose_name="Send email on artist comments"
    )
    email_on_group_add = models.BooleanField(
        default=True, verbose_name="Send email on group/label approval"
    )
    email_on_pm = models.BooleanField(default=True, verbose_name="Send email on new PM")
    fave_id = models.IntegerField(
        blank=True,
        null=True,
        verbose_name="Fave SongID",
        help_text="SongID number of *the* song you love the most!",
    )
    # ~ group = models.ForeignKey(Group, null = True, blank = True)
    infoline = models.CharField(blank=True, max_length=50)
    info = models.TextField(
        blank=True,
        verbose_name="Profile Info",
        help_text="Enter a little bit about yourself. No HTML. BBCode tags allowed",
    )
    last_active = models.DateTimeField(blank=True, null=True)
    last_activity = models.DateTimeField(blank=True, null=True, db_index=True)
    paginate_favorites = models.BooleanField(default=True)
    pm_accepted_upload = models.BooleanField(
        default=True, verbose_name="Send PM on accepted upload"
    )
    show_screenshots = models.BooleanField(
        default=True, verbose_name="Show Screenshots/Pouet Images in Currently Playing"
    )
    show_youtube = models.BooleanField(
        default=True, verbose_name="Show YouTube videoes in Currently Playing"
    )
    # ~ theme = models.ForeignKey(Theme, blank = True, null = True)
    token = models.CharField(blank=True, max_length=32)
    use_tags = models.BooleanField(verbose_name="Show tags", default=True)
    visible_to = models.CharField(max_length=1, default="A", choices=VISIBLE_TO)
    web_page = models.URLField(
        blank=True,
        verbose_name="Website",
        help_text="Your personal website address. Must be a valid URL",
    )
    hellbanned = models.BooleanField(default=False)
    # ~ links = generic.GenericRelation(GenericLink)

    def __str__(self):
        return self.user.username


class Theme(models.Model):
    title = models.CharField(max_length=20)
    active = models.BooleanField(default=True)
    description = models.TextField(blank=True)
    css = models.CharField(max_length=120)
    default = models.BooleanField(default=False)
    creator = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    # ~ screenshots = generic.GenericRelation("ScreenshotObjectLink")

    class Meta:
        ordering = ["-default", "title"]

    def __str__(self):
        return self.title


class Logo(models.Model):
    file = models.FileField(upload_to="logos")
    active = models.BooleanField(default=True, db_index=True)
    creator = models.CharField(max_length=60)
    description = models.TextField(blank=True)
    legacy = models.BooleanField(default=False)

    def __str__(self):
        return self.file.name
