from django.contrib import admin

from .models import Logo, News, Status, Theme, UserProfile


@admin.register(Logo)
class LogoAdmin(admin.ModelAdmin):
    pass


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    date_hierarchy = "created_at"
    list_display = ["created_at", "status", "title"]


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    list_display = ["code", "name"]


@admin.register(Theme)
class ThemeAdmin(admin.ModelAdmin):
    pass


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass
